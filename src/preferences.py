from gi.repository import Gtk, Adw, GObject

from .utils import pmos

@Gtk.Template(resource_path='/org/postmarketos/gpmbootstrap/ui/preferences.ui')
class GpmbootstrapPreferences(Adw.PreferencesWindow):
    __gtype_name__ = 'GpmbootstrapPreferences'

    channels_dropdown = Gtk.Template.Child()
    recovery_zip_switch = Gtk.Template.Child()
    ondev_switch = Gtk.Template.Child()
    reboot_switch = Gtk.Template.Child()

    def __init__(self, **kwargs):
        super().__init__()
        self.connect('close-request', self.close_window)
        self.channels_dropdown.connect("map", self.load_channels)
        # On-device installer is not supported with android recovery zip
        self.recovery_zip_switch.bind_property("active", self.ondev_switch, "sensitive", GObject.BindingFlags.INVERT_BOOLEAN)
        self.ondev_switch.bind_property("active", self.recovery_zip_switch, "sensitive", GObject.BindingFlags.INVERT_BOOLEAN)

    def close_window(self, user_data):
        self.hide()
        return True

    def change_channel(self, user_data, pspec):
        # Switch to selected channel and reload devices

        if "selected-item" in str(pspec):
            pmos.set_channel(user_data.get_selected_item().get_string())
            self.get_transient_for().load_devices("")

    def load_channels(self, user_data):
        # Load release channels of postmarketos

        channels = Gtk.StringList()
        select = 0
        for i, channel in enumerate(pmos.get_channels()):
            channels.append(channel)
            if channel == pmos.get_current_channel():
                select = i
        self.channels_dropdown.set_model(channels)
        self.channels_dropdown.set_selected(select)
        self.channels_dropdown.connect("notify", self.change_channel)
