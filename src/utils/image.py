import requests
from bs4 import BeautifulSoup
import pmb

def list_folders(url):
  response = requests.get(url)
  soup = BeautifulSoup(response.text, 'html.parser')
  folders = [li.text for li in soup.find_all('li', class_='dir')]
  return folders

def list_files(url):
  response = requests.get(url)
  soup = BeautifulSoup(response.text, 'html.parser')
  files_raw = [li.text for li in soup.find_all('li', class_='file')]
  files = []
  for file in files_raw:
    files.append(file.split("\n")[0])
  return files

def get_images_url(device, release, ui):
  url = f"https://images.postmarketos.org/bpo/{release}/{device}/{ui}"
  build = list_folders(url)[0].replace("latest", '').replace("\n", '')
  url += f"/{build}/"
  return url

def get_image_downloadable(device, release, ui):
  releases = list_folders("https://images.postmarketos.org/bpo/")
  if release in releases:
    url = f"https://images.postmarketos.org/bpo/{release}/"
    devices = list_folders(url)
    if device in devices:
      url = f"https://images.postmarketos.org/bpo/{release}/{device}/"
      uis = list_folders(url)
      if ui in uis:
        return True
  return False

def download_images(device, release, ui, argssubstitute):
  url = get_images_url(device, release, ui)
  pmb.chroot.apk.install(argssubstitute, ["xz"])
  pmb.chroot.root(argssubstitute, ["mkdir", "-p", "/home/pmos/rootfs", f"/mnt/chroot_{device}"])
  for file in list_files(url):
    file_url = url + file
    print(file_url)
    download_to = f"/home/pmos/rootfs/{device}"
    out = f"/home/pmos/rootfs/{device}"
    if "boot" in file:
      download_to += "-boot.img.xz"
      out += "-boot.img"
    else:
      download_to += "-root.img.xz"
      out += "-root.img"
    pmb.chroot.root(argssubstitute, ["wget", file_url, "-O", download_to])
    pmb.chroot.root(argssubstitute, ["rm", "-f", out])
    pmb.chroot.root(argssubstitute, ["xz", "--decompress", download_to])
    if "boot" in file:
      pmb.chroot.root(argssubstitute, ["cp", out, f"/mnt/chroot_{device}/boot"])
    else:
      pmb.chroot.root(argssubstitute, ["cp", out, f"/home/pmos/rootfs/{device}.img"])
