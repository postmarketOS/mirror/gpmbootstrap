import pmb
import os
import argparse
from . import image

# A type of string that returns itself when iterating it
class IterableString:
    def __init__(self, string):
        self.string = string

    def __repr__(self):
        return self.string

    def __getitem__(self, item):
        if item == 0:
            return self.string

    def __iter__(self):
        yield self.string

class ArgsSubstitute:
    def __init__(self):
        self.config = pmb.config.defaults["config"]
        self.aports = self.work + "/cache_git/pmaports"
        self.mirrors_postmarketos = IterableString(self.mirrors_postmarketos)
        self.timeout = 180 # 3 minutes
        self.details_to_stdout = True
        self.verbose = False
        self.device = None
        self.kernel = None
        self.ui = None
        self.ui_extras = False
        self.offline = False
        self.is_default_channel = True
        self.selected_providers = []
        self.action_flasher = ""
        self.autoinstall = True
        pmb.config.merge_with_args(self)

    def __iter__(self):
          for each in vars(self):
              yield each

    def __getattr__(self, name):
        # If the attribute dose not exist, try to get it from config

        config = pmb.config.load(self)
        try:
            return config.get("pmbootstrap", name)
        except:
            return None

class InstallArgsSubstitute:
    password = ""
    user = ""
    hostname = ""
    android_recovery_zip = False
    cipher = "none"
    full_disk_encryption = False
    on_device_installer = False

    def __getattr__(self, name):
        # If the attribute is not found, try to find it in the install subparser

        parser = argparse.ArgumentParser(prog="pmbootstrap")
        sub = parser.add_subparsers(title="action", dest="action")
        pmb.parse.arguments_install(sub)
        for item in sub.__dict__["_name_parser_map"]["install"].__dict__["_actions"]:
            option = item.dest
            while option[0] == "-":
                option = option[1:]
            if option.replace("-", "_") == name:
                return item.default
        try:
            return getattr(argssubstitute, name)
        except:
            return None

    def __iter__(self):
      for each in vars(self):
          yield (each)

argssubstitute = ArgsSubstitute()
installargssubstitute = InstallArgsSubstitute()

def init(pmb_sudo="pkexec"):
    os.environ["PMB_SUDO"] = pmb_sudo

    pmb.helpers.logging.init(argssubstitute)
    pmb.helpers.other.init_cache()
    pmb.config.pmaports.clone(argssubstitute)

def get_channels():
    return pmb.helpers.git.parse_channels_cfg(argssubstitute)["channels"]

def set_channel(channel):
    pmb.config.pmaports.switch_to_channel_branch(argssubstitute, channel)
    argssubstitute.is_default_channel = False

def get_current_channel():
    return pmb.config.pmaports.read_config(argssubstitute)["channel"]

def get_devices():
    # Returns a list of supported devices

    device_blacklist = ["qemu-aarch64", "qemu-amd64", "qemu-riscv64"]
    codenames = pmb.helpers.devices.list_codenames(argssubstitute)
    devices = []
    for device in codenames:
        if device in device_blacklist:
            continue
        deviceinfo = pmb.parse.deviceinfo(argssubstitute, device)
        devices.append({device: deviceinfo["name"]})
    return devices

def set_device(device_codename):
    argssubstitute.device = device_codename
    deviceinfo = pmb.parse.deviceinfo(argssubstitute, device_codename)
    argssubstitute.deviceinfo = deviceinfo
    argssubstitute.arch = deviceinfo["arch"]

def get_kernels():
    pmb.helpers.other.init_cache()
    return pmb.parse._apkbuild.kernels(argssubstitute, argssubstitute.device)

def set_kernel(kernel):
    argssubstitute.kernel = kernel

def get_uis(device):
    ui_list = pmb.helpers.ui.list(argssubstitute, argssubstitute.arch)
    accelerated = "gpu_accelerated" in argssubstitute.deviceinfo and argssubstitute.deviceinfo["gpu_accelerated"] == "true"
    if not accelerated:
        for i in reversed(range(len(ui_list))):
            pkgname = f"postmarketos-ui-{ui_list[i][0]}"
            apkbuild = pmb.helpers.pmaports.get(argssubstitute, pkgname, subpackages=False, must_exist=False)
            if apkbuild and "pmb:gpu-accel" in apkbuild["options"]:
                ui_list.pop(i)
    return ui_list

def set_ui(ui):
    argssubstitute.ui = ui

def get_flash_actions():
    # Returns a list of supported flash actions

    parser = argparse.ArgumentParser(prog="pmbootstrap")
    sub = parser.add_subparsers(title="action", dest="action")
    pmb.parse.arguments_flasher(sub)
    all_flash_actions = []
    for item in sub.__dict__["_name_parser_map"]["flasher"].__dict__["_actions"]:
        if item.dest == "action_flasher":
            all_flash_actions = item.choices
    flash_actions = []
    flash_action_blacklist = ["boot", "list_devices", "list_flavors", "flash_lk2nd"]
    vars = pmb.flasher.variables(argssubstitute, None, argssubstitute.deviceinfo["flash_method"])
    for action in all_flash_actions:
        if get_flash_method() != "adb":
            if action in argssubstitute.deviceinfo["partition_blacklist"].split(","):
                continue
            if action == "flash_vbmeta" and not vars["$PARTITION_VBMETA"]:
                continue
            if action == "flash_dtbo" and not vars["$PARTITION_DTBO"]:
                continue
            if action in flash_action_blacklist:
                continue
            if action not in pmb.config.flashers[get_flash_method()]["actions"]:
                continue
        elif action != "sideload":
                continue
        flash_actions.append(action)
    return flash_actions

def get_flash_method():
    return argssubstitute.deviceinfo["flash_method"]

def is_device_connected():
    # Returns true if the device is connected, false or none otherwise

    try:
        pmb.flasher.init(argssubstitute)
    except:
        return None
    if get_flash_method() == "none" or get_flash_method() is None:
        return None
    for flasher in pmb.config.flashers:
        if flasher == get_flash_method():
            try:
                device = pmb.chroot.root(argssubstitute, pmb.config.flashers[flasher]["actions"]["list_devices"][0], output_return = True)
                return device.splitlines()[-1] != "" or (device.__contains__("sideload") and get_flash_method() == "adb")
            except:
                return False
    return False

def download_images(window):
    image.download_images(argssubstitute.device, get_current_channel(), argssubstitute.ui, argssubstitute)
    window.start_flashing()

def flash(action):
    argssubstitute.action_flasher = action
    pmb.flasher.frontend(argssubstitute)

def install(window):
    # Zap chroots and install postmarketos

    cfg = pmb.config.load(argssubstitute)
    for item in vars(argssubstitute):
        value = getattr(argssubstitute, item)
        if value is None:
            cfg["pmbootstrap"][item] = ""
        else:
            cfg["pmbootstrap"][item] = str(value)

    pmb.config.save(argssubstitute, cfg)
    pmb.chroot.zap(installargssubstitute, False)
    pmb.helpers.frontend.install(installargssubstitute)
    window.start_flashing()
