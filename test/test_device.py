from gpmbootstrap import utils


def init():
    utils.pmos.init("sudo")

def test_device():
    assert len(utils.pmos.get_devices()) >= 2
    utils.pmos.set_device("pine64-pinephone")
    assert len(utils.pmos.get_uis(utils.pmos.argssubstitute.device_codename)) >= 3

init()